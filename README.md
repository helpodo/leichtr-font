# Symetics Font

## Usage

**In your gemfile**

    gem 'leichtr-font', git: 'git@bitbucket.org:helpodo/leichtr-font.git'

**Run**

    bundle install


**Include it in your scss file**

    @import "leichtr-font";


### Helper

    leichtr_icon 'symetics', 'Text..', class: 'logo'


## Updating

### Installation

**First we will need to install a few things**

```bash
brew install fontforge --with-python
brew install eot-utils
```

**Clone this repo with**

    git clone git@bitbucket.org:helpodo/leichtr-font.git

**And run**

    bundle install

**Compile Fonts**

    fontcustom compile


### Release new version

    rake push:[patch, minor, major]

