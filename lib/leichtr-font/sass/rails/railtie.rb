require 'leichtr-font/sass/rails/helpers'

module LeichtrFont
  module Sass
    module Rails
      class Railtie < ::Rails::Railtie
       initializer 'leichtr-font.view_helpers' do
          ActionView::Base.send :include, ViewHelpers
        end
      end
    end
  end
end
