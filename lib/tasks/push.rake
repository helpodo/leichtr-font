desc 'Pushing changes and release'

task :push do
  puts 'Please specify the version width:'
  puts 'rake push:patch'
  puts 'rake push:minor'
  puts 'rake push:major'
end

namespace :push do
  task :patch do
    desc 'Releasing patch'
    puts 'Releasing patch...'
    Rake::Task['compile'].invoke
    system 'gem bump --version patch --tag --push'
  end

  task :minor do
    desc 'Releasing minor version'
    puts 'Releasing minor version...'
    Rake::Task['compile'].invoke
    system 'gem bump --version minor --tag --push'
  end

  task :major do
    desc 'Releasing major version'
    puts 'Releasing major version...'
    Rake::Task['compile'].invoke
    system 'gem bump --version major --tag --push'
  end
end
