# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'leichtr-font/version'

Gem::Specification.new do |spec|
  spec.name          = 'leichtr-font'
  spec.version       = LeichtrFont::VERSION
  spec.authors       = ['Tobias Block']
  spec.email         = ['tobias.block@symetics.com']
  spec.description   = 'LeichtrFont gem for use in Ruby projects'
  spec.summary       = 'LeichtrFont'
  spec.homepage      = 'https://bitbucket.org/helpodo/leichtr-font'
  #spec.license       = 'MIT'
  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'sass', '~> 3.2'

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'sass-rails'
  spec.add_development_dependency 'compass'

  spec.metadata['allowed_push_host'] = ''
end
